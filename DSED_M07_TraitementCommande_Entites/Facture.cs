﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSED_M07_TraitementCommande_Entites
{
    public class Facture
    {
        public List<Article> Articles { get; set; }
        public double Taxes = 0.09975d;
        public double TotalSansTaxes { get; set; }
        public double TotalAvecTaxes { get; set; }
        public Facture()
        {
            Articles = new List<Article>();
        }
    }
}
