﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSED_M07_TraitementCommande_Entites
{
    public class Article
    {
        public Guid Reference { get; set; }
        public string Nom { get; set; }
        public double Prix { get; set; }
        public int Quantite { get; set; }
        public Article()
        {
            ;
        }
        public override string ToString()
        {
            return $"Reference: {this.Reference.ToString()}\r\nNom: {this.Nom}\r\nPrix: {this.Prix.ToString()}\r\nQuantite: {this.Quantite}";
        }
    }
}
