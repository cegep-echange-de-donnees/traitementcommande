﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSED_M07_TraitementCommande_Entites
{
    public class Commande
    {
        public string Nom { get; set; }
        public Guid Reference { get; set; }
        public List<Article> Articles { get; set; }
        public Commande()
        {
            this.Articles = new List<Article>();
        }
        public override string ToString()
        {
            string chaine = $"Nom client: {this.Nom}\r\nReference: {this.Reference.ToString()}\r\nArticles:\r\n";

            foreach(Article article in this.Articles)
            {
                chaine = chaine + Environment.NewLine + article.ToString();
            }

            return chaine;
        }
    }
}
