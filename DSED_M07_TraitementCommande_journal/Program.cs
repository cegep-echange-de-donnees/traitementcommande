﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.IO;

namespace DSED_M07_TraitementCommande_journal
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] requetesSujets = { "#" };
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Out.WriteLine("=== File de message: m07-journal ===");
            Console.Out.WriteLine();

            ConnectionFactory factory = new ConnectionFactory() { HostName = "localhost" };
            using (IConnection connection = factory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(
                    exchange: "m07-commandes",
                    type: "topic",
                    durable: true,
                    autoDelete: false
                    );

                    channel.QueueDeclare(
                    "m07-journal",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null
                    );

                    foreach (var requeteSujet in requetesSujets)
                    {
                        channel.QueueBind(queue: "m07-journal", exchange: "m07-commandes",
                        routingKey: requeteSujet);
                    }

                    EventingBasicConsumer consumateur = new EventingBasicConsumer(channel);
                    consumateur.Received += (model, ea) =>
                    {
                        byte[] body = ea.Body.ToArray();
                        string message = Encoding.UTF8.GetString(body);
                        string sujet = ea.RoutingKey;

                        string? path = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.Parent?.GetDirectories("Journaux")?
                                                                                                                 .Select(directory => directory)?
                                                                                                                 .SingleOrDefault()?.FullName;

                        string nomFichier = DateTime.Now.ToLongDateString().Replace("-", "") + "_" + DateTime.Now.ToLongTimeString().Replace(":", "") + "_" + Guid.NewGuid().ToString() + ".json";

                        using (StreamWriter file = new StreamWriter(path + "\\" + nomFichier, append: true))
                        {
                            file.WriteAsync(message);
                        }

                        Console.Out.WriteLine();
                        Console.WriteLine("{0} Message reçu {1} avec le sujet : {2}", DateTime.Now.ToShortTimeString(), message, sujet);
                    };

                    channel.BasicConsume(queue: "m07-journal",
                    autoAck: true,
                    consumer: consumateur);
                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
            }
        }
    }
}
