﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using DSED_M07_TraitementCommande_Entites;

namespace DSED_M07_TraitementCommande_facturation
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] requetesSujets = { "commande.placee.*" };
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Out.WriteLine("=== File de message: m07-facturation ===");
            Console.Out.WriteLine();

            ConnectionFactory factory = new ConnectionFactory() { HostName = "localhost" };
            using (IConnection connection = factory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(
                    exchange: "m07-commandes",
                    type: "topic",
                    durable: true,
                    autoDelete: false
                    );

                    channel.QueueDeclare(
                    "m07-facturation",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null
                    );

                    foreach (var requeteSujet in requetesSujets)
                    {
                        channel.QueueBind(queue: "m07-facturation", exchange: "m07-commandes",
                        routingKey: requeteSujet);
                    }

                    EventingBasicConsumer consumateur = new EventingBasicConsumer(channel);
                    consumateur.Received += (model, ea) =>
                    {
                        byte[] body = ea.Body.ToArray();
                        string message = Encoding.UTF8.GetString(body);
                        string sujet = ea.RoutingKey;

                        Commande? commande = JsonConvert.DeserializeObject<Commande>(message);

                        if(commande != null)
                        {
                            Facture facture = new Facture();

                            foreach(Article article in commande.Articles)
                            {
                                facture.Articles.Add(article);
                                facture.TotalSansTaxes = facture.TotalSansTaxes + (article.Prix * article.Quantite);
                            }

                            if(sujet.Split(".", StringSplitOptions.None)[2] == "premium")
                            {
                                facture.TotalSansTaxes = facture.TotalSansTaxes - (facture.TotalSansTaxes * 0.05d);
                            }

                            facture.TotalAvecTaxes = facture.TotalSansTaxes * facture.Taxes;

                            string factureJson = JsonConvert.SerializeObject(facture);

                            string? path = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.Parent?.GetDirectories("Factures")?
                                                                                                                 .Select(directory => directory)?
                                                                                                                 .SingleOrDefault()?.FullName;

                            string nomFichier = DateTime.Now.ToLongDateString().Replace("-", "") + "_" + DateTime.Now.ToLongTimeString().Replace(":", "") + "_" + commande.Reference.ToString() + "_Facture" + ".json";

                            using (StreamWriter file = new StreamWriter(path + "\\" + nomFichier, append: true))
                            {
                                file.WriteAsync(factureJson);
                            }

                            Console.Out.WriteLine();
                            Console.WriteLine("{0} Message traite {1} avec le sujet : {2}", DateTime.Now.ToShortTimeString(), factureJson, sujet);
                        }
                    };

                    channel.BasicConsume(queue: "m07-facturation",
                    autoAck: true,
                    consumer: consumateur);
                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
            }
        }
    }
}
