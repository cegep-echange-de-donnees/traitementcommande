﻿using RabbitMQ.Client;
using Newtonsoft.Json;
using System.Text;
using DSED_M07_TraitementCommande_Entites;

namespace DSED_M07_TraitementCommande_producteur
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Out.WriteLine("=== Echange: m07-commandes ===");
            Console.Out.WriteLine();

            ConnectionFactory factory = new ConnectionFactory() { HostName = "localhost" };
            using (IConnection connection = factory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(
                    exchange: "m07-commandes",
                    type: "topic",
                    durable: true,
                    autoDelete: false
                    );

                    int nombreMessages = 10;
                    string[] types = new string[] { "normal", "premium" };
                    string[] nomsArticles = new string[] { "crayon", "feuille de papier", "cahier", "gomme", "regles" };
                    string[] nomsClients = new string[] { "Bob", "Marc", "John", "Ali", "Chang", "Alice", "Georges", "Sylvie" };
                    Random random = new Random();

                    for (int i = 0; i < nombreMessages; i++)
                    {
                        string commande = "commande";
                        string statut = "placee";
                        string type = types[random.Next(types.Length)];
                        string sujet = $"{commande}.{statut}.{type}";

                        int nbArticle = random.Next(5);

                        Commande nouvelleCommande = new Commande();
                        nouvelleCommande.Nom = nomsClients[random.Next(nomsClients.Length)];
                        nouvelleCommande.Reference = Guid.NewGuid();

                        for (int j = 0; j < nbArticle; j++)
                        {
                            Article article = new Article();
                            article.Nom = nomsArticles[random.Next(nomsArticles.Length)];
                            article.Reference = Guid.NewGuid();
                            article.Prix = random.Next(12) + random.NextDouble();
                            article.Quantite = random.Next(15);
                            nouvelleCommande.Articles.Add(article);
                        }

                        string message = JsonConvert.SerializeObject(nouvelleCommande);
                        var body = Encoding.UTF8.GetBytes(message);

                        channel.BasicPublish(exchange: "m07-commandes",
                        routingKey: sujet,
                        basicProperties: null,
                        body: body);

                        Console.Out.WriteLine(commande.ToString());
                    }
                }
            }
        }
    }
}
