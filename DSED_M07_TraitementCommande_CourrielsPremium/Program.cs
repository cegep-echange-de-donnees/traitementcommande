﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using DSED_M07_TraitementCommande_Entites; 

namespace DSED_M07_TraitementCommande_CourrielsPremium
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] requetesSujets = { "commande.placee.premium" };
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Out.WriteLine("=== File de message: m07-courriel-premium ===");
            Console.Out.WriteLine();

            ConnectionFactory factory = new ConnectionFactory() { HostName = "localhost" };
            using (IConnection connection = factory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(
                    exchange: "m07-commandes",
                    type: "topic",
                    durable: true,
                    autoDelete: false
                    );

                    channel.QueueDeclare(
                    "m07-courriel-premium",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null
                    );

                    foreach (var requeteSujet in requetesSujets)
                    {
                        channel.QueueBind(queue: "m07-courriel-premium", exchange: "m07-commandes",
                        routingKey: requeteSujet);
                    }

                    EventingBasicConsumer consumateur = new EventingBasicConsumer(channel);
                    consumateur.Received += (model, ea) =>
                    {
                        byte[] body = ea.Body.ToArray();
                        string message = Encoding.UTF8.GetString(body);
                        string sujet = ea.RoutingKey;

                        Commande? commande = JsonConvert.DeserializeObject<Commande>(message);

                        if (commande != null)
                        {
                            Console.Out.WriteLine();
                            Console.Out.WriteLine("Commande premium: " + commande.Reference.ToString());
                        }
                    };

                    channel.BasicConsume(queue: "m07-courriel-premium",
                    autoAck: true,
                    consumer: consumateur);
                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
            }
        }
    }
}
